This patch is based on the upstream commit described below, adapted for use in
the Debian package by Peter Michael Green.

commit dee559021800e204221ddea220fd7c1de6864851
Author: Andreas Molzer <andreas.molzer@gmx.de>
Date:   Wed Jan 26 23:24:16 2022 +0100

    Succeed early on unusual resize quickcheck case
    
    The quickcheck now exits early when checking resizing when it runs into
    the case where we are limited by `u32::MAX`. These cases should still be
    handled fine but in general should probably be covered by a separate
    test. In particular we can no longer expect the new size to be exact in
    the requested dimension.

--- a/src/math/utils.rs
+++ b/src/math/utils.rs
@@ -9,3 +9,3 @@
 /// aspect ratio), or will shrink so that both dimensions are
-/// completely contained with in the given `width` and `height`,
+/// completely contained within the given `width` and `height`,
 /// with empty space on one axis.
@@ -46,4 +46,7 @@
             if old_w == 0 || new_w == 0 { return true; }
+            if new_w as u64 * 400u64 >= old_w as u64 * u64::from(u32::MAX) { return true; }
+
             let result = super::resize_dimensions(old_w, 400, new_w, ::std::u32::MAX, false);
-            result.0 == new_w && result.1 == (400 as f64 * new_w as f64 / old_w as f64) as u32
+            let exact = (400 as f64 * new_w as f64 / old_w as f64) as u32;
+            result.0 == new_w && result.1 == exact.max(1)
         }
@@ -54,4 +57,7 @@
             if old_h == 0 || new_h == 0 { return true; }
+            if 400u64 * new_h as u64 >= old_h as u64 * u64::from(u32::MAX) { return true; }
+
             let result = super::resize_dimensions(400, old_h, ::std::u32::MAX, new_h, false);
-            result.1 == new_h && result.0 == (400 as f64 * new_h as f64 / old_h as f64) as u32
+            let exact = (400 as f64 * new_h as f64 / old_h as f64) as u32;
+            result.1 == new_h && result.0 == exact.max(1)
         }
