This patch is based on the upstream commit described below, with changes
to files not present in the Debian package removed and to remove mentions
of F-I-X-M-E (remove dashes) which upsets the Debian cargo tools.

commit acd3aef63607d8c1b256943f9f53e7bfb7680352
Author: gnzlbg <gonzalobg88@gmail.com>
Date:   Thu Feb 7 13:47:26 2019 +0100

    Fix clippy issues

diff --git a/src/api/cast.rs b/src/api/cast.rs
index 70ce4615a..f1c32ca1a 100644
--- a/src/api/cast.rs
+++ b/src/api/cast.rs
@@ -1,11 +1,5 @@
 //! Implementation of `FromCast` and `IntoCast`.
-#![cfg_attr(
-    feature = "cargo-clippy",
-    allow(
-        clippy::module_name_repetitions,
-        clippy::stutter
-    )
-)]
+#![allow(clippy::module_name_repetitions)]
 
 /// Numeric cast from `T` to `Self`.
 ///
diff --git a/src/api/cmp/partial_eq.rs b/src/api/cmp/partial_eq.rs
index 7e2782f0f..1712a0de5 100644
--- a/src/api/cmp/partial_eq.rs
+++ b/src/api/cmp/partial_eq.rs
@@ -34,12 +34,13 @@ macro_rules! impl_cmp_partial_eq {
             }
         }
 
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _cmp_PartialEq>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn partial_eq() {
                         let a = $id::splat($false);
                         let b = $id::splat($true);
diff --git a/src/api/fmt/binary.rs b/src/api/fmt/binary.rs
index c758dadcc..b60769082 100644
--- a/src/api/fmt/binary.rs
+++ b/src/api/fmt/binary.rs
@@ -3,11 +3,10 @@
 macro_rules! impl_fmt_binary {
     ([$elem_ty:ident; $elem_count:expr]: $id:ident | $test_tt:tt) => {
         impl crate::fmt::Binary for $id {
-            #[cfg_attr(
-                feature = "cargo-clippy", allow(clippy::missing_inline_in_public_items)
-            )]
-            fn fmt(&self, f: &mut crate::fmt::Formatter<'_>)
-                   -> crate::fmt::Result {
+            #[allow(clippy::missing_inline_in_public_items)]
+            fn fmt(
+                &self, f: &mut crate::fmt::Formatter<'_>,
+            ) -> crate::fmt::Result {
                 write!(f, "{}(", stringify!($id))?;
                 for i in 0..$elem_count {
                     if i > 0 {
@@ -18,12 +17,13 @@ macro_rules! impl_fmt_binary {
                 write!(f, ")")
             }
         }
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _fmt_binary>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn binary() {
                         use arrayvec::{ArrayString,ArrayVec};
                         type TinyString = ArrayString<[u8; 512]>;
diff --git a/src/api/fmt/debug.rs b/src/api/fmt/debug.rs
index f34a237d9..ad0b8a59a 100644
--- a/src/api/fmt/debug.rs
+++ b/src/api/fmt/debug.rs
@@ -2,12 +2,13 @@
 
 macro_rules! impl_fmt_debug_tests {
     ([$elem_ty:ty; $elem_count:expr]: $id:ident | $test_tt:tt) => {
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _fmt_debug>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn debug() {
                         use arrayvec::{ArrayString,ArrayVec};
                         type TinyString = ArrayString<[u8; 512]>;
@@ -42,10 +43,7 @@ macro_rules! impl_fmt_debug_tests {
 macro_rules! impl_fmt_debug {
     ([$elem_ty:ty; $elem_count:expr]: $id:ident | $test_tt:tt) => {
         impl crate::fmt::Debug for $id {
-            #[cfg_attr(
-                feature = "cargo-clippy",
-                allow(clippy::missing_inline_in_public_items)
-            )]
+            #[allow(clippy::missing_inline_in_public_items)]
             fn fmt(
                 &self, f: &mut crate::fmt::Formatter<'_>,
             ) -> crate::fmt::Result {
diff --git a/src/api/fmt/lower_hex.rs b/src/api/fmt/lower_hex.rs
index ac6ea9593..5a7aa14b5 100644
--- a/src/api/fmt/lower_hex.rs
+++ b/src/api/fmt/lower_hex.rs
@@ -3,11 +3,10 @@
 macro_rules! impl_fmt_lower_hex {
     ([$elem_ty:ident; $elem_count:expr]: $id:ident | $test_tt:tt) => {
         impl crate::fmt::LowerHex for $id {
-            #[cfg_attr(
-                feature = "cargo-clippy", allow(clippy::missing_inline_in_public_items)
-            )]
-            fn fmt(&self, f: &mut crate::fmt::Formatter<'_>)
-                   -> crate::fmt::Result {
+            #[allow(clippy::missing_inline_in_public_items)]
+            fn fmt(
+                &self, f: &mut crate::fmt::Formatter<'_>,
+            ) -> crate::fmt::Result {
                 write!(f, "{}(", stringify!($id))?;
                 for i in 0..$elem_count {
                     if i > 0 {
@@ -18,12 +17,13 @@ macro_rules! impl_fmt_lower_hex {
                 write!(f, ")")
             }
         }
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _fmt_lower_hex>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn lower_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
                         type TinyString = ArrayString<[u8; 512]>;
diff --git a/src/api/fmt/octal.rs b/src/api/fmt/octal.rs
index ff78a332b..83ac8abc7 100644
--- a/src/api/fmt/octal.rs
+++ b/src/api/fmt/octal.rs
@@ -3,11 +3,10 @@
 macro_rules! impl_fmt_octal {
     ([$elem_ty:ident; $elem_count:expr]: $id:ident | $test_tt:tt) => {
         impl crate::fmt::Octal for $id {
-            #[cfg_attr(
-                feature = "cargo-clippy", allow(clippy::missing_inline_in_public_items)
-            )]
-            fn fmt(&self, f: &mut crate::fmt::Formatter<'_>)
-                   -> crate::fmt::Result {
+            #[allow(clippy::missing_inline_in_public_items)]
+            fn fmt(
+                &self, f: &mut crate::fmt::Formatter<'_>,
+            ) -> crate::fmt::Result {
                 write!(f, "{}(", stringify!($id))?;
                 for i in 0..$elem_count {
                     if i > 0 {
@@ -18,12 +17,13 @@ macro_rules! impl_fmt_octal {
                 write!(f, ")")
             }
         }
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _fmt_octal>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn octal_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
                         type TinyString = ArrayString<[u8; 512]>;
diff --git a/src/api/fmt/upper_hex.rs b/src/api/fmt/upper_hex.rs
index ab60c7e22..aa88f673a 100644
--- a/src/api/fmt/upper_hex.rs
+++ b/src/api/fmt/upper_hex.rs
@@ -3,11 +3,10 @@
 macro_rules! impl_fmt_upper_hex {
     ([$elem_ty:ident; $elem_count:expr]: $id:ident | $test_tt:tt) => {
         impl crate::fmt::UpperHex for $id {
-            #[cfg_attr(
-                feature = "cargo-clippy", allow(clippy::missing_inline_in_public_items)
-            )]
-            fn fmt(&self, f: &mut crate::fmt::Formatter<'_>)
-                   -> crate::fmt::Result {
+            #[allow(clippy::missing_inline_in_public_items)]
+            fn fmt(
+                &self, f: &mut crate::fmt::Formatter<'_>,
+            ) -> crate::fmt::Result {
                 write!(f, "{}(", stringify!($id))?;
                 for i in 0..$elem_count {
                     if i > 0 {
@@ -18,12 +17,13 @@ macro_rules! impl_fmt_upper_hex {
                 write!(f, ")")
             }
         }
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _fmt_upper_hex>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn upper_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
                         type TinyString = ArrayString<[u8; 512]>;
diff --git a/src/api/minimal/iuf.rs b/src/api/minimal/iuf.rs
index ed85cf769..58ffabab9 100644
--- a/src/api/minimal/iuf.rs
+++ b/src/api/minimal/iuf.rs
@@ -18,8 +18,7 @@ macro_rules! impl_minimal_iuf {
             /// Creates a new instance with each vector elements initialized
             /// with the provided values.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::too_many_arguments))]
+            #[allow(clippy::too_many_arguments)]
             pub const fn new($($elem_name: $elem_ty),*) -> Self {
                 Simd(codegen::$id($($elem_name as $ielem_ty),*))
             }
diff --git a/src/api/minimal/mask.rs b/src/api/minimal/mask.rs
index 18ca5b24f..e65be95db 100644
--- a/src/api/minimal/mask.rs
+++ b/src/api/minimal/mask.rs
@@ -16,16 +16,14 @@ macro_rules! impl_minimal_mask {
             /// Creates a new instance with each vector elements initialized
             /// with the provided values.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::too_many_arguments))]
+            #[allow(clippy::too_many_arguments)]
             pub const fn new($($elem_name: bool),*) -> Self {
                 Simd(codegen::$id($(Self::bool_to_internal($elem_name)),*))
             }
 
             /// Converts a boolean type into the type of the vector lanes.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::indexing_slicing))]
+            #[allow(clippy::indexing_slicing)]
             const fn bool_to_internal(x: bool) -> $ielem_ty {
                 [0 as $ielem_ty, !(0 as $ielem_ty)][x as usize]
             }
diff --git a/src/api/minimal/ptr.rs b/src/api/minimal/ptr.rs
index 38b8a73c9..75e5aad5c 100644
--- a/src/api/minimal/ptr.rs
+++ b/src/api/minimal/ptr.rs
@@ -19,8 +19,7 @@ macro_rules! impl_minimal_p {
             /// Creates a new instance with each vector elements initialized
             /// with the provided values.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::too_many_arguments))]
+            #[allow(clippy::too_many_arguments)]
             pub const fn new($($elem_name: $elem_ty),*) -> Self {
                 Simd(codegen::$id($($elem_name),*))
             }
@@ -89,8 +88,7 @@ macro_rules! impl_minimal_p {
                           it returns a new vector with the value at `index` \
                           replaced by `new_value`d"
             ]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::not_unsafe_ptr_arg_deref))]
+            #[allow(clippy::not_unsafe_ptr_arg_deref)]
             pub fn replace(self, index: usize, new_value: $elem_ty) -> Self {
                 assert!(index < $elem_count);
                 unsafe { self.replace_unchecked(index, new_value) }
@@ -210,8 +208,7 @@ macro_rules! impl_minimal_p {
         }
 
         impl<T> crate::fmt::Debug for $id<T> {
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::missing_inline_in_public_items))]
+            #[allow(clippy::missing_inline_in_public_items)]
             fn fmt(&self, f: &mut crate::fmt::Formatter<'_>)
                    -> crate::fmt::Result {
                 write!(
@@ -424,7 +421,7 @@ macro_rules! impl_minimal_p {
             }
         }
 
-        #[cfg_attr(feature = "cargo-clippy", allow(clippy::partialeq_ne_impl))]
+        #[allow(clippy::partialeq_ne_impl)]
         impl<T> crate::cmp::PartialEq<$id<T>> for $id<T> {
             #[inline]
             fn eq(&self, other: &Self) -> bool {
@@ -621,8 +618,7 @@ macro_rules! impl_minimal_p {
             #[inline]
             pub unsafe fn from_slice_aligned_unchecked(slice: &[$elem_ty])
                                                        -> Self {
-                #[cfg_attr(feature = "cargo-clippy",
-                           allow(clippy::cast_ptr_alignment))]
+                #[allow(clippy::cast_ptr_alignment)]
                 *(slice.get_unchecked(0) as *const $elem_ty as *const Self)
             }
 
@@ -840,8 +836,7 @@ macro_rules! impl_minimal_p {
             pub unsafe fn write_to_slice_aligned_unchecked(
                 self, slice: &mut [$elem_ty],
             ) {
-                #[cfg_attr(feature = "cargo-clippy",
-                           allow(clippy::cast_ptr_alignment))]
+                #[allow(clippy::cast_ptr_alignment)]
                 *(slice.get_unchecked_mut(0) as *mut $elem_ty as *mut Self) =
                     self;
             }
@@ -1225,8 +1220,7 @@ macro_rules! impl_minimal_p {
             /// are difficult to satisfy. The only advantage of this method is
             /// that it enables more aggressive compiler optimizations.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::should_implement_trait))]
+            #[allow(clippy::should_implement_trait)]
             pub unsafe fn add(self, count: $usize_ty) -> Self {
                 self.offset(count.cast())
             }
@@ -1271,8 +1265,7 @@ macro_rules! impl_minimal_p {
             /// are difficult to satisfy. The only advantage of this method is
             /// that it enables more aggressive compiler optimizations.
             #[inline]
-            #[cfg_attr(feature = "cargo-clippy",
-                       allow(clippy::should_implement_trait))]
+            #[allow(clippy::should_implement_trait)]
             pub unsafe fn sub(self, count: $usize_ty) -> Self {
                 let x: $isize_ty = count.cast();
                 // note: - is currently wrapping_neg
diff --git a/src/api/reductions/mask.rs b/src/api/reductions/mask.rs
index 13d713067..0dd6a84e7 100644
--- a/src/api/reductions/mask.rs
+++ b/src/api/reductions/mask.rs
@@ -20,12 +20,13 @@ macro_rules! impl_reduction_mask {
             }
         }
 
-        test_if!{
+        test_if! {
             $test_tt:
             paste::item! {
                 pub mod [<$id _reduction>] {
                     use super::*;
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn all() {
                         let a = $id::splat(true);
                         assert!(a.all());
@@ -43,7 +44,8 @@ macro_rules! impl_reduction_mask {
                             }
                         }
                     }
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn any() {
                         let a = $id::splat(true);
                         assert!(a.any());
@@ -61,7 +63,8 @@ macro_rules! impl_reduction_mask {
                             }
                         }
                     }
-                    #[cfg_attr(not(target_arch = "wasm32"), test)] #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
+                    #[cfg_attr(not(target_arch = "wasm32"), test)]
+                    #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn none() {
                         let a = $id::splat(true);
                         assert!(!a.none());
diff --git a/src/api/slice/from_slice.rs b/src/api/slice/from_slice.rs
index ca83c7df7..109cd1f10 100644
--- a/src/api/slice/from_slice.rs
+++ b/src/api/slice/from_slice.rs
@@ -53,10 +53,7 @@ macro_rules! impl_slice_from_slice {
                     0
                 );
 
-                #[cfg_attr(
-                    feature = "cargo-clippy",
-                    allow(clippy::cast_ptr_alignment)
-                )]
+                #[allow(clippy::cast_ptr_alignment)]
                 *(target_ptr as *const Self)
             }
 
diff --git a/src/api/slice/write_to_slice.rs b/src/api/slice/write_to_slice.rs
index becb564d4..fcb288da7 100644
--- a/src/api/slice/write_to_slice.rs
+++ b/src/api/slice/write_to_slice.rs
@@ -55,20 +55,10 @@ macro_rules! impl_slice_write_to_slice {
                     0
                 );
 
-                                #[cfg_attr(feature = "cargo-clippy",
-                                           allow(clippy::cast_ptr_alignment))]
-                        #[cfg_attr(
-                            feature = "cargo-clippy",
-                            allow(clippy::cast_ptr_alignment)
-                        )]
-                #[cfg_attr(
-                    feature = "cargo-clippy",
-                    allow(clippy::cast_ptr_alignment)
-                )]
-                #[cfg_attr(
-                    feature = "cargo-clippy",
-                    allow(clippy::cast_ptr_alignment)
-                )]
+                                #[allow(clippy::cast_ptr_alignment)]
+                        #[allow(clippy::cast_ptr_alignment)]
+                #[allow(clippy::cast_ptr_alignment)]
+                #[allow(clippy::cast_ptr_alignment)]
                 *(target_ptr as *mut Self) = self;
             }
 
diff --git a/src/codegen/math/float.rs b/src/codegen/math/float.rs
index 02a2bea08..5e89bf6ae 100644
--- a/src/codegen/math/float.rs
+++ b/src/codegen/math/float.rs
@@ -1,5 +1,5 @@
 //! Vertical floating-point math operations.
-#![cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+#![allow(clippy::useless_transmute)]
 
 #[macro_use]
 crate mod macros;
diff --git a/src/codegen/swap_bytes.rs b/src/codegen/swap_bytes.rs
index 15d47f68e..b435fb5da 100644
--- a/src/codegen/swap_bytes.rs
+++ b/src/codegen/swap_bytes.rs
@@ -24,7 +24,7 @@ macro_rules! impl_swap_bytes {
         $(
             impl SwapBytes for $id {
                 #[inline]
-                #[cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+                #[allow(clippy::useless_transmute)]
                 fn swap_bytes(self) -> Self {
                     unsafe {
                         let bytes: u8x4 = crate::mem::transmute(self);
@@ -39,7 +39,7 @@ macro_rules! impl_swap_bytes {
         $(
             impl SwapBytes for $id {
                 #[inline]
-                #[cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+                #[allow(clippy::useless_transmute)]
                 fn swap_bytes(self) -> Self {
                     unsafe {
                         let bytes: u8x8 = crate::mem::transmute(self);
@@ -56,7 +56,7 @@ macro_rules! impl_swap_bytes {
         $(
             impl SwapBytes for $id {
                 #[inline]
-                #[cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+                #[allow(clippy::useless_transmute)]
                 fn swap_bytes(self) -> Self {
                     unsafe {
                         let bytes: u8x16 = crate::mem::transmute(self);
@@ -74,7 +74,7 @@ macro_rules! impl_swap_bytes {
         $(
             impl SwapBytes for $id {
                 #[inline]
-                #[cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+                #[allow(clippy::useless_transmute)]
                 fn swap_bytes(self) -> Self {
                     unsafe {
                         let bytes: u8x32 = crate::mem::transmute(self);
@@ -94,7 +94,7 @@ macro_rules! impl_swap_bytes {
         $(
             impl SwapBytes for $id {
                 #[inline]
-                #[cfg_attr(feature = "cargo-clippy", allow(clippy::useless_transmute))]
+                #[allow(clippy::useless_transmute)]
                 fn swap_bytes(self) -> Self {
                     unsafe {
                         let bytes: u8x64 = crate::mem::transmute(self);
diff --git a/src/lib.rs b/src/lib.rs
index b27e9ace2..d73645e72 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -214,25 +214,17 @@
     crate_visibility_modifier,
     custom_inner_attributes
 )]
-#![allow(non_camel_case_types, non_snake_case)]
-#![cfg_attr(test, feature(hashmap_internals))]
-#![cfg_attr(
-    feature = "cargo-clippy",
-    allow(
-        clippy::cast_possible_truncation,
-        clippy::cast_lossless,
-        clippy::cast_possible_wrap,
-        clippy::cast_precision_loss,
-        // This lint is currently broken for generic code
-        // See https://github.com/rust-lang/rust-clippy/issues/3410
-        clippy::use_self
-    )
-)]
-#![cfg_attr(
-    feature = "cargo-clippy",
-    deny(clippy::missing_inline_in_public_items)
+#![allow(non_camel_case_types, non_snake_case,
+         clippy::cast_possible_truncation,
+         clippy::cast_lossless,
+         clippy::cast_possible_wrap,
+         clippy::cast_precision_loss,
+         // This lint is currently broken for generic code
+         // See https://github.com/rust-lang/rust-clippy/issues/3410
+         clippy::use_self
 )]
-#![deny(warnings, rust_2018_idioms)]
+#![cfg_attr(test, feature(hashmap_internals))]
+#![deny(warnings, rust_2018_idioms, clippy::missing_inline_in_public_items)]
 #![no_std]
 
 use cfg_if::cfg_if;
@@ -288,10 +280,7 @@ pub struct Simd<A: sealed::SimdArray>(
 /// and/or `Ord` traits.
 #[repr(transparent)]
 #[derive(Copy, Clone, Debug)]
-#[cfg_attr(
-    feature = "cargo-clippy",
-    allow(clippy::missing_inline_in_public_items)
-)]
+#[allow(clippy::missing_inline_in_public_items)]
 pub struct LexicographicallyOrdered<T>(T);
 
 mod masks;
diff --git a/src/masks.rs b/src/masks.rs
index 65c15cc5d..f83c4da95 100644
--- a/src/masks.rs
+++ b/src/masks.rs
@@ -36,7 +36,7 @@ macro_rules! impl_mask_ty {
             }
         }
 
-        #[cfg_attr(feature = "cargo-clippy", allow(clippy::partialeq_ne_impl))]
+        #[allow(clippy::partialeq_ne_impl)]
         impl PartialEq<$id> for $id {
             #[inline]
             fn eq(&self, other: &Self) -> bool {
