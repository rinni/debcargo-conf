commit a8273dd9900075f8d8fc5a583d2f1c4b1a0b5b14
Author: ecstatic-morse <ecstaticmorse@gmail.com>
Date:   Thu Feb 20 13:42:30 2020 -0800

    Mark `simd_shuffle` intrinsics as `rustc_args_required_const`
    
    This change was made in `stdarch` but not `packed_simd`. See rust-lang/rust#69280 for background.

diff --git a/src/codegen/llvm.rs b/src/codegen/llvm.rs
index 6ee30c590..93c6ce6b7 100644
--- a/src/codegen/llvm.rs
+++ b/src/codegen/llvm.rs
@@ -11,29 +10,35 @@ extern "platform-intrinsic" {
     // out-of-bounds will produce a monomorphization-time error.
     // https://github.com/rust-lang-nursery/packed_simd/issues/21
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle2<T, U>(x: T, y: T, idx: [u32; 2]) -> U
     where
         T: Simd,
         <T as Simd>::Element: Shuffle<[u32; 2], Output = U>;
 
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle4<T, U>(x: T, y: T, idx: [u32; 4]) -> U
     where
         T: Simd,
         <T as Simd>::Element: Shuffle<[u32; 4], Output = U>;
 
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle8<T, U>(x: T, y: T, idx: [u32; 8]) -> U
     where
         T: Simd,
         <T as Simd>::Element: Shuffle<[u32; 8], Output = U>;
 
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle16<T, U>(x: T, y: T, idx: [u32; 16]) -> U
     where
         T: Simd,
         <T as Simd>::Element: Shuffle<[u32; 16], Output = U>;
 
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle32<T, U>(x: T, y: T, idx: [u32; 32]) -> U
     where
         T: Simd,
         <T as Simd>::Element: Shuffle<[u32; 32], Output = U>;
 
+    #[rustc_args_required_const(2)]
     pub fn simd_shuffle64<T, U>(x: T, y: T, idx: [u32; 64]) -> U
     where
